//Se importa la biblioteca para la lectura de datos
import java.util.Scanner;

//Se crea la clase
public class ATM{
    //Se declara el arreglo
    Cuenta usuarios[];

    //Se crea el constructor sin parametros
    public ATM(){

        //Se inicializa el arreglo
        usuarios = new Cuenta[10];

        //Se introducen los 10 usuarios
        usuarios[0] = new Cuenta (0, 100000);
        usuarios[1] = new Cuenta (1, 100000);
        usuarios[2] = new Cuenta (2, 100000);
        usuarios[3] = new Cuenta (3, 100000);
        usuarios[4] = new Cuenta (4, 100000);
        usuarios[5] = new Cuenta (5, 100000);
        usuarios[6] = new Cuenta (6, 100000);
        usuarios[7] = new Cuenta (7, 100000);
        usuarios[8] = new Cuenta (8, 100000);
        usuarios[9] = new Cuenta (9, 100000);

        //Se crea el objeto Scanner
        Scanner entrada = new Scanner(System.in);

        //Se crean la variables que seran utilizadas
        int opcion;
        int monto;

        boolean salir;
        boolean loop = true;

        double deposito;
        double retiro;

        //Se inicializa el while 
        while(loop == true){
        
            //Se pide el usuario ingresar su Id
            System.out.println("Ingrese su Id:");
            monto = entrada.nextInt();
     
            if (monto >= 0 && monto < 10) {
     
              salir = false;
     
              while(salir == false){
     
                //Se impirme en pantalla el menu
                 System.out.println(" ");
                 System.out.println("Menu Principal");
                 System.out.println(" ");
                 System.out.println("1. Ver el balance actual");
                 System.out.println("2. Retirar Dinero");
                 System.out.println("3. Depositar Dinero");
                 System.out.println("4. Salir");
                 System.out.println(" ");
               
               //Solicita al usuario una opcion
                 System.out.println("--------------------");
                 System.out.println("Ingrese su selección. ");
                 opcion = entrada.nextInt();      
                 
                 //Se hacen los casos del switch
                 switch(opcion){

                    //En el caso 1 se le da el balance al usuario
                    case 1:
                    System.out.println(" ");
                    System.out.print("El balance es de: ");
                    System.out.println(usuarios[monto].get_Balance());
                    break;

                    //En el caso 2 se retira dinero de la cuenta
                   case 2:
                     System.out.println("Ingrese una cantidad para retirar");
                     retiro = entrada.nextDouble();
                     usuarios[monto].retirarDinero(retiro);
                     break;

                    //En el caso 3 se deposita dinero en la cuenta
                   case 3:
                     System.out.println("Ingrese una cantidad para depositar");
                     deposito = entrada.nextDouble();
                     usuarios[monto].depositarDinero(deposito);
                     break;

                    //En el caso 4 se sale del prgrama
                   case 4:
                     System.out.println("Salio del programa");
                     salir=true;
                     break;

                    //Se imprime un mensaje para avisar que no se dio una opcion valida
                   default:
                     System.out.println("Opcion no valida");
                 }
            }
        }           
     }

    }

}
    

    


