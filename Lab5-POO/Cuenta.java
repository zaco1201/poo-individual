import java.time.*;

//Se crea la clase cuenta con los atributos privados
public class Cuenta{
    private int id;
    private double balance;
    private double tasaDeInteresAnual;
    private LocalDate fechaDeCreacion;

    //Se crea el constructor sin argumentos de la cuenta
    public Cuenta(){
    }

    //Constructor que toma los argumentos de Id y Balance
    public Cuenta(int id, double balance){
        this.id = id;
        this.balance = balance;
    }

    //Hacemos los metodos Get y Set para Id
    public void set_Id(int id){
        this.id = id;
    }

    public int get_Id(){
        return id;
    }

    // //Hacemos los metodos Get y Set para Balance
    public void set_Balance(double balance){
        this.balance = balance;
    }
    public double get_Balance(){
        return balance;    
    }

    // //Hacemos los metodos Get y Set para TasaDeInteresAnual
    public void set_TasaDeInteresAnual(double tasaDeInteresAnual){
        this.tasaDeInteresAnual = tasaDeInteresAnual;
    }
    public double get_TasaDeInteresAnual(){
        return tasaDeInteresAnual;    
    }

    // //Hacemos el metodo Get para acceder a FechaDeCreacion
    public LocalDate get_FechaDeCreacion(){
        return fechaDeCreacion;
    }

    //Se crea un set para agregar la fecha
    public void set_FechaDeCreacion(LocalDate fechaCreacion){
         this.fechaDeCreacion = fechaCreacion;
    }

    //Metodo que retorna la taza de interes mensual
    public double tasaDeInteresMensual(double tasaDeInteresAnual){
        double tasaDeInteresMensual = tasaDeInteresAnual / 12;
        return tasaDeInteresMensual;
    }

    // //Metodo retirar dinero que disminuye la cantidad de dinero de la cuenta
    public double retirarDinero(double MontoRetirado){
        this.balance = balance - MontoRetirado; //Se le resta al Balance el monto que el usuario retira 
        return this.balance; 
    }

    //Metodo depositar dinero que aumenta la cantidad de dinero de la cuenta
    public double depositarDinero(double montoDepositado){
        this.balance = balance + montoDepositado; //Se le suma a Balance el monto que el usuario deposita
        return balance;
    }
} 
   