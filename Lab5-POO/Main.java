import java.time.*;

public class Main{
    //Funcion Main
    public static void main(String[] args){
        //Se imprime en pantalla un mensaje de Bienvenida
        System.out.println(" ");
        System.out.println("Bienvenido");

        //Se crea el objeto cuenta de tipo Cuenta y se le agrega un Id, un Balance y una Tasa de interes anual
        Cuenta cuenta = new Cuenta();
        cuenta.set_Id(1122);
        cuenta.set_Balance(500000);
        cuenta.set_TasaDeInteresAnual(0.045);
    
        //Se imprime en pantalla los detalles de la cuenta
        System.out.print("Id: ");
        System.out.println(cuenta.get_Id());   
        System.out.print("Balace: ");
        System.out.println(cuenta.get_TasaDeInteresAnual());
        System.out.print("Tasa de interes anual: ");
        System.out.println(cuenta.get_TasaDeInteresAnual());
        
        //Se utiliza el metodo DepositarDinero para depositar 150.000CRC al Balance
        System.out.print("Se depositaron: ");
        System.out.println(cuenta.depositarDinero(150000));

        // //Se utiliza el metodo RetirarDinero para retirar 200.000CRC al Balance
        System.out.print("Se retiraron: ");
        System.out.println(cuenta.retirarDinero(200000));

        // //Se imprime en pantalla en Balance
        System.out.print("Se tiene un Balance de: ");
        System.out.println(cuenta.get_Balance());

        //Se imprime en pantalla el interes mensual
        System.out.println("Se tiene una tasa de interes mensual de: ");
        System.out.println(cuenta.tasaDeInteresMensual(cuenta.get_TasaDeInteresAnual()));

        //Se imprime en pantalla la fecha cuando fue creada la cuenta
        System.out.print("La cuenta fue creada en: ");
        cuenta.set_FechaDeCreacion(LocalDate.parse("2020-05-31"));
        System.out.println(cuenta.get_FechaDeCreacion());

        //Se crea la segunda cuenta
        System.out.println(" ");
        System.out.println("Segunda cuenta");

        //Se crea el objeto
		Cuenta cuenta2 = new Cuenta();
		cuenta2.set_Balance(1000);
		cuenta2.set_TasaDeInteresAnual(0.306);
		LocalDate creaciónCuenta2 = LocalDate.parse("2020-05-31");
        
        // //Se imprime en pantalla en Balance
		System.out.print("Balance: ");
		System.out.println(cuenta2.get_Balance());
        
        //Se imprime en pantalla el interes mensual
		System.out.print("Tasa de interes mensual de: ");
		System.out.println(cuenta2.tasaDeInteresMensual(cuenta2.get_TasaDeInteresAnual()));
        
        //Se imprime en pantalla la fecha cuando fue creada la cuenta
		System.out.print("La cuenta fue creada en: ");
        System.out.println(creaciónCuenta2);
        System.out.println(" ");

            //Se crea el constructor del ATM
    ATM cajero = new ATM();
    }


}