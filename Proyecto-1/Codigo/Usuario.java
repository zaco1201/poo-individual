
public class Usuario {
	//Se crean los atributos privados
	private String Nombre;
	private String usuario;
	private String contraseña;
	
	//Se crea el constructor
	public Usuario(String nombre, String usuario, String contraseña) {;
		this.Nombre = nombre;
		this.usuario = usuario;
		this.contraseña = contraseña;
	}
	
	//Se crean los metodos get y set encesarios
	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public boolean verificarContraseña(String contraseña){
        return this.contraseña == contraseña;
        }

	
}
