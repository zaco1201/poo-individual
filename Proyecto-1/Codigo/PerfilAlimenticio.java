
public class PerfilAlimenticio {
	//Se crean los atributos privados
	private boolean vegano;
	private boolean vegetariano;
	private boolean libreGluten;
	private boolean halal;
	private boolean kosher;
	private String alergiasointolerancias;
	
	//Se crea el constructor
	public PerfilAlimenticio(boolean vegano, boolean vegetariano, boolean libreGluten, boolean halal, boolean kosher, String alergiasointolerancias) {
		this.vegano = vegano;
		this.vegetariano = vegetariano;
		this.libreGluten = libreGluten;
		this.halal = halal;
		this.kosher = kosher;
		this.alergiasointolerancias = alergiasointolerancias;
	}
	
	//Se crean los metodos get y set necesarios
	public boolean getVegano() {
		return vegano;
	}
	
	public void setVegano(boolean vegano) {
		this.vegano = vegano;
	}

	public boolean getVegetariano() {
		return vegetariano;
	}

	public void setVegetariano(boolean vegetariano) {
		this.vegetariano = vegetariano;
	}
	
	public boolean getLibreGluten() {
		return libreGluten;
	}
	
	public void setLibreGluten(boolean libreGluten) {
		this.libreGluten = libreGluten;
	}

	public boolean getHalal() {
		return halal;
	}

	public void setHalal(boolean halal) {
		this.halal = halal;
	}

	public boolean getKosher() {
		return kosher;
	}
	
	public void setKosher(boolean kosher) {
		this.kosher = kosher;
	}
	
	public String getalergiasointolerancias() {
		return alergiasointolerancias;
	}
	
	public void setalergiasointolerancias(String alergiasointolerancias) {
		this.alergiasointolerancias = alergiasointolerancias;
	}
	
}
