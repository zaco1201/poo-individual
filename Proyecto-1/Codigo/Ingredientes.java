

public class Ingredientes {
	//Se crean los atributos privados
	private String nombreIngredientes;
    private String medida;
    private float costo;
    
    //Se crea el constructor
    public Ingredientes(String nombreIngredientes, String medida, float costo) {
    	this.nombreIngredientes = nombreIngredientes;
    	this.medida = medida;
    	this.costo = costo;
    }
    
    //Se crean los metodos get y set necesarios
    public String getnombreIngredientes() {
    	return nombreIngredientes;
    }
    
    public void setnombreIngredientes(String nombreIngredientes) {
    	this.nombreIngredientes = nombreIngredientes;
    }
    
    public String getmedida() {
    	return medida;
    }
    
    public void setmedida(String medida) {
    	this.medida = medida;
    }
    
    public float getcosto() {
    	return costo;
    }
    
    public void setcosto(float costo) {
    	this.costo = costo;
    }
}
  