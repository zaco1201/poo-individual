import java.util.ArrayList;

public class Cliente extends Usuario {
	//Se crean los atributos privados
	private String numeroTelefono;
	private int montoPagar;
	private PerfilAlimenticio perfilAlimenticio;
	private ArrayList<DireccionEntrega> direccionEntrega;
	
	//Se crea el constructor
	public Cliente(String nombre, String usuario, String contraseņa, String numeroTelefono, int montoPagar, PerfilAlimenticio perfilAlimenticio, ArrayList<DireccionEntrega> direccionEntrega) {
		super(nombre, usuario, contraseņa);
		this.numeroTelefono = numeroTelefono;
		this.montoPagar = montoPagar;
		this.perfilAlimenticio = perfilAlimenticio;
		this.direccionEntrega = direccionEntrega;
	}
	
	//Se crean los metodos get y set necesarios
	public String getnumeroTelefono() {
		return numeroTelefono;
	}
	
	public void setnumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}
	
	public int getmontoPagar() {
		return montoPagar;
	}
	
	public void setmontoPagar(int montoPagar) {
		this.montoPagar = montoPagar;
	}
	
	public PerfilAlimenticio getperfilAlimenticio() {
		return perfilAlimenticio;
	}
	
	public void setperfilAlimenticio(PerfilAlimenticio perfilAlimenticio) {
		this.perfilAlimenticio = perfilAlimenticio;
	}
	
	public ArrayList<DireccionEntrega> getdireccionEntrega() {
	    return direccionEntrega;
	}

	public void setDondeRecojerPedido(ArrayList<DireccionEntrega> dondeRecojerPedido) {
		this.direccionEntrega = direccionEntrega;
	}
		
}
