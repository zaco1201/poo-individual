import java.util.ArrayList;

public class Pedido {
	//Se crean los atributos privados
    private ArrayList<Ingredientes> ingredientes;
    private String nombrePedido;
    private String receta;
    private int precio;
    
    //Se crea el constructor
    public Pedido(ArrayList<Ingredientes> ingredientes, String nombrePedido, String receta, int precio) {
        this.ingredientes = ingredientes;
        this.nombrePedido = nombrePedido;
        this.receta = receta;
        this.precio = precio;
    }

    //Se crean los metodos get y set necesarios
    public ArrayList<Ingredientes> getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(ArrayList<Ingredientes> ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getNombrePedido() {
        return nombrePedido;
    }

    public void setNombrePedido(String nombrePedido) {
        this.nombrePedido = nombrePedido;
    }

    public String getReceta() {
        return receta;
    }

    public void setReceta(String receta) {
        this.receta = receta;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}