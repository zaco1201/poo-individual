
public class DireccionEntrega {
	//Se crean los atributos privados
    private String nombre;
    private float longitud;
    private float latitud;

    //Se crea el constructor
    public DireccionEntrega(String nombre, float longitud, float latitud) {
        this.nombre = nombre;
        this.longitud = longitud;
        this.longitud = latitud;
    }

    //Se crean los metodos get y set necesarios
    public String getnombre() {
        return nombre;
    }

    public void setnombre(String nombre) {
        this.nombre = nombre;
    }

    public float getlongitud() {
        return longitud;
    }

    public void setlongitud(float longitud) {
        this.longitud = longitud;
    }

    public float getlatitud() {
        return latitud;
    }

    public void setlatitud(float latitud) {
        this.latitud = latitud;
    }

    //Se crea el m�todo toString
    @Override
    public String toString() {
        return "La direccion de entrega es en: " + nombre + "\n" + "longitud: " + longitud +"\n"+ "latitud: " + latitud;
    }
       
}
