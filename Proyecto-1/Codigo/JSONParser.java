
import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class JSONParser {
  private File file;
  private ObjectMapper mapper;
  private JsonNode nodo;
  
public JSONParser() {
    
    file = new File ("resources/data.json");
    mapper = new ObjectMapper();
    
    try {
      nodo = mapper.readTree(file);
    } catch (JsonProcessingException e) {
      
      e.printStackTrace();
    } catch (IOException e) {
      
      e.printStackTrace();
    }
    
}


