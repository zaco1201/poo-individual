import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    JSONParser parser = new JSONParser();
    Controlador controlador = parser.cargarAparatos();
    boolean bandera = true;
    
    while(bandera) {
      
      System.out.println("");
      System.out.println("  Casa Inteligente \n");
      System.out.println(" 1. Visualizar aparatos \n");
      System.out.println(" 2. Enceder Electrodomestico \n");
      System.out.println(" 3. Apagar Electrodomestico \n");
      System.out.println(" 4. Salir \n");
      System.out.println("Seleccione una opcion: \n");
      Scanner entrada = new Scanner(System.in);
      
      int opcion;
      
      opcion = entrada.nextInt();
      
      switch(opcion) {
      
      case 1:
        
        System.out.println(controlador);
        
        break;
        
      case 2:
        
        int choiceOn;
        
        System.out.println("Digite el ID de aparato que quiere encender \n");
        
        choiceOn = entrada.nextInt();
        
        if(controlador.size() < choiceOn) {
          
          System.out.println("No aparece\n");
          
        }
        
        else {
          
          controlador.get(choiceOn).encender();
          
        }
        
        
        break;
        
      case 3:
        
        int choiceOff;
        
        System.out.println("Digite el ID de aparato que quiere apagar \n");
        
        choiceOff = entrada.nextInt();
        
        if(controlador.size() < choiceOff) {
          
          System.out.println("No aparece\n");
          
        }
        
        else {
          
          controlador.get(choiceOff).apagar();
          
        }
        
        break;
        
        
      case 4:
        
        bandera = false;
        break;
      
      }
      

      
    }
    


  }
  }