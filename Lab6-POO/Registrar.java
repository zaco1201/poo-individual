import java.util.Scanner;
import java.util.ArrayList;

//Se crea el constructo de la clase Registrar
public class Registrar{

    private Usuario cuentaActual;    
    private ArrayList<Usuario> usuarioOij;
    private ArrayList<Usuario> usuarioComun;

    public Registrar(){
        this.usuarioOij = new ArrayList<>();
        this.usuarioComun = new ArrayList<>();

    }
    
    //Se crea el constructor con los parametros
    public void agregarUsuarioOij(int carnet, String puesto, String nombre, int cedula, int id, String password,String tipo){
        
        usuarioOij.add(new OIJ(carnet, puestoTrabajo, nombre, identificacion, id, contraseña, "OIJ"));
    }
    
    public void agregarCuentaPersona(String nombre, int cedula, int id, String password,String tipo){
        
        usuarioComun.add(new Persona(nombre, identificacion, id, contraseña, "Persona"));

    }
    
    public int buscar(int id, String tipo){
        
        if("OIJ".equals(tipo)){                                           
            for(int i = 0; i<usuarioOij.size(); i++){
                if(usuarioOij.get(i).getId() == id){
                    return i;
                    
                }
            }
        } 
        
        else { 
            for (int i = 0; i < usuarioComun.size(); i++){
                if(usuarioComun.get(i).getId() == id){ 
                    return i;
                
                }
            
            }
        }
        
        return 100;                                                              
    }
    
    public Usuario getUsuario(int posicion, String tipo){
        
        if("OIJ".equals(tipoCuenta)){
            
            return cuentaOij.get(posicion);
  
        }
        
        else{
            
            return usuarioComun.get(posicion);
            
        }

    }
    
    public void iniciarSesion(int ID){
        
        Scanner entrada = new Scanner (System.in);
        
        String contraseña = "";
        
        if(buscar(ID,"OIJ") < 100){
            
                this.cuentaActual = getUsuario(buscarUsuario(ID,"OIJ"),"OIJ");
                System.out.println("Ingrese su contrasena: ");
                contraseña = entrada.next(contraseña);
            
                 
        
                while(!this.cuentaActual.verificarContraseña(contraseña)){
                System.out.println("Error con la contraseña");
                System.out.println("Ingrese de nuevo la contraseña");
            
                }
        
                System.out.println("Ingreso al sistema\n");
        
                System.out.println("Hola \n" + this.cuentaActual.getNombre());
    }
        else if (buscar(ID,"Persona") < 100){
            
                 this.cuentaActual = getUsuario(buscar(ID,"Persona"),"Persona");
                 
                System.out.println("Ingrese su contraseña: ");
                contraseña = entrada.next(contraseña);
                
                while(!this.cuentaActual.verificarContraseña(contraseña)){
                System.out.println("Error con la contraseña");
                System.out.println("Ingrese de nuevo la contraseña");
            
                }
        }
                
        else{
                        
                System.out.println("Ingreso un ID incorrecto");
                System.out.println("Ingrese de nuevo su ID");
                ID = entrada.nextInt();
                iniciarSesion(ID);
 
                }

        }


}