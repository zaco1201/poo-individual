import java.time.*;

public class Demanda {
    
    private String cuerpo;
    private LocalDate fecha;
    private int hora;
    

    //Se crea el constructor de la clase Demanda con los parametros
    public Demanda(String cuerpo, LocalDate fecha, int hora) {
        this.cuerpo = cuerpo;
        this.fecha = fecha;
        this.hora = hora;
    }

    //Metodos get y set
    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getHora() {
        return hora;
    }

    public void toStringDemanda(){
        return fecha + "\n" + hora + "\n" + descricion;
    }
    
}

