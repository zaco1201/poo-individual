import java.time.*;

public class DemandaGeo extends Demanda{
    
    private int latitud;
    private int longitud;
    
    //Se crea el constructor
    public DemandaGeo(int latitud, int longitud, String descripcion, LocalDate fecha, int hora) {
        super(descripcion, fecha, hora);
        this.latitud = latitud;
        this.longitud = longitud;
    }
    
    public void setLatitud(int latitud) {
        this.latitud = latitud;
    }

    public int getLatitud() {
        return latitud;
    }

    public int getLongitud() {
        return longitud;
    }
    

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }
    
    public void toStringDenunciaGeo (){
        
        System.out.println("Descripcion de los hechos \n " + getDescripcion());
        System.out.println("Fecha\n " + getFecha());
        System.out.println("Hora\n " + getHora());
        System.out.println("Latitud\n " + getLatitud());
        System.out.println("Longitud\n " + getLongitud());

        return getFecha() + "\n" + getHora() + "\n" + getDecripcion;
    }
    
}
