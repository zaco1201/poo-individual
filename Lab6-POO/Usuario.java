import java.util.ArrayList;
//Se crea la super clase Usuario con los atributos privados
public class Usuario{
    private int id;
    private String contraseña;
    private String nombre;
    private int identificacion;
    private Usuario usuario;

    //Se crea el constructor con los parametros
    public Usuario(int id, String contraseña, String nombre, int identificacion){
        this.id = id;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.identificacion = identificacion;
    }

    //Metodos get y set
    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setContraseña(String contraseña){
        this.contraseña = contraseña;
    }

    public String getContraseña(){
        return contraseña;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public setIdentificacion(int identificacion){
        this.identificacion = identificacion;
    }

    public int getIdentificacion(){
        return identificacion;
    }

    //public setUsuario(Usuario usuario){
        //this.usuario = usuario;
    //}

    //public Usuario getUsuario(){
        //return usuario;
    //}

    //Metodo para verificar la contraseña introducida
    public boolean verificacionContraseña(String contraseña){
        return this.contraseña == contraseña;
    }

    //Metodo toString
    public String toStringUsuario(){
        return "Nombre: "+ getNombre() + "\n" + "Identificacion: " + getIdentificacion() + "\n" + "Id: " + getId();
    }


}










