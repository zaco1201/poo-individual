//Se crea el constructor de la clase UsuarioComun con los parametros
public class UsuariComun extends Usuario{

    //Se crea el constructor con los parametros
    public UsuariComun(int id, String contraseña, String nombre, int identificacion){
        super(id, contraseña, nombre, identificacion);
    }
    
    //Metodo toSting
    public String toStringUsuarioComun (){
        return "Nombre: "+ getNombre() + "\n" + "Identificacion: " + getIdentificacion();
    }

}
