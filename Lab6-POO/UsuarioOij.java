import java.util.ArrayList;

 //Se crea la clase de herencia con los atributos privados
public class UsuarioOij extends Usuario{
    private int carnet;
    private String puestoTrabajo;

    //Se crean los arreglos
    ArrayList<Demanda> = new ArrayList();
    ArrayList<DemandaGeo> = new ArrayList();

    //Se crea el constructor de la clase UsuarioOij con los parametros
    public UsuarioOij(int carnet, String puestTrabajo, int id, String contraseña, String nombre, int identificacion){
        super(id, contraseña, nombre, identificacion);
        this.carnet = carnet;
        this.puestoTrabajo = puestTrabajo;

    }

    //Metodos get y set
    public ArrayList<Demanda> getDemanda(){
        return this.Demanda;
    }

    public ArrayList<DemandaGeo> getDemandaGeo(){
        return this.DemandaGeo;
    }

    public void setCarnet(int carnet){
        this.carnet = carnet;
    }

    public int getCarnet(){
        return carnet;
    }

    public void setPuestoTrabajo(String puestoTrabajo){
        this.puestoTrabajo = puestoTrabajo;
    }

    public int getPuestoTrabajo(){
        return puestoTrabajo;
    }

    //Metood para poder ver la demanda
    public void verDemanda(){
        if (Demanda.isEmpy()){
            System.out.println("No hay");
        }
        
        for(int i = 0; i<Demanda.size(); i++){
            
            System.out.println("------------------------------------------------------------------------\nDemanda: \n "+(i+1));
            System.out.println(Demanda.get(i).getCuerpoDemanda());
            System.out.println("Fecha: " + Demanda.get(i).getFecha());
            System.out.println("Hora: " + Demanda.get(i).getHora());}

        }

        public void verDemandaGeo(){
        
            if(DemandaGeo.isEmpty()){
                
                System.out.println("No hay");
           
            }
            
            for(int i = 0; i < DemandaGeo.size(); i++){
                
                System.out.println("------------------------------------------------------------------------\nDemanda: \n" + (i+1));
                System.out.println(DemandaGeo.get(i).getCuerpoDemanda());
                System.out.println("Fecha: " + DemandaGeo.get(i).getFecha());
                System.out.println("Hora: " + DemandaGeo.get(i).getHora());
                System.out.println("Latitud: " + DemandaGeo.get(i).getLatitud());
                System.out.println("Longitud: " + DemandaGeo.get(i).getLongitud());
            
            }
        
        }

    //Metodo toSting
    public String toStringUsuarioOij(){
         return "Nombre: "+ getNombre() + "\n" + "Identificacion: " + getIdentificacion() + "\n" + "Id: " + getId() + "\n" + "Carnet: " + getCarnet() + "\n" + "Puesto de trabajo: " + getPuestoTrabajo();
    }
    
}
